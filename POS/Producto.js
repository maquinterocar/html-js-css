class Producto {
    static CONTADOR = 0;
    _ID;
    _nombre;
    _precio;

    constructor(nombre, precio = 0) {
        this._ID = ++ Producto.CONTADOR;
        this._nombre = nombre;
        this._precio = precio;
    }

    get ID() {
        return this._ID;
    }

    get nombre() {
        return this._nombre;
    }

    set nombre(value) {
        this._nombre = value;
    }

    get precio() {
        return this._precio;
    }

    set precio(value) {
        this._precio = value;
    }

    toString(){
        return `ID: ${this._ID}\nNombre: ${this._nombre}\nPrecio: $ ${this._precio}`;
    }

}