class Orden {
    static contador = 0;
    _orden_ID
    _productos
    _productos_contador = 0;

    get productos_contador() {
        return this._productos_contador;
    }

    set productos_contador(value) {
        this._productos_contador = value;
    }

    get productos() {
        return this._productos;
    }

    items(){
        let items = {};
        for (let item of this.productos){
            const formato = `${item.ID} ${item.nombre}`.toLowerCase();
            if (!items.hasOwnProperty(formato)){
                items[formato] = item;
                items[formato]["#"] = 1;
                this._productos_contador++;
                continue;
            }
            items[formato]["#"]++;
        }
        console.log(items);
        return Object.values(items);
    }

    set productos(value) {
        this._productos = value;
    }

    static get MAX_PRODUCTOS() {
        return 5;
    }

    constructor() {
        this._orden_ID = ++Orden.contador;
        this._productos = [];
        // this._productos_contador = 0;
    }

    add(producto){
        if(this._productos.length < Orden.MAX_PRODUCTOS){
            this.productos.push(producto);
            return this.productos;
        }
        console.log('Has llegado al límite de productos');
        return this.productos;
    }

    total(){
        let total = 0;
        for (let producto of this.productos){
            if(typeof producto.precio != "undefined"){
                total += producto.precio;
            }
        }
        return total;
    }

    ver(){
        let print = "";
        for (let producto of this.productos){
            if(producto != null){
                print += `- ${producto.toString()}\n`;
            }
        }
        print = `${this.verID()}\nProductos:\n${print}\n${this.verTotal()}`;
        return print;
    }

    total_items() {
        return `# Items: ${this.productos_contador}`;
    }

    ver_total() {
        return `Total: $ ${this.total()}`;
    }

    ver_ID() {
        return `No.Orden: ${this._orden_ID}`;
    }
}