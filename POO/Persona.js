class Persona{
    static _ID
    _nombre
    _apellido
    _edad
    static contador = 0

    get nombre() {
        return this._nombre;
    }

    set nombre(value) {
        this._nombre = value;
    }

    get apellido() {
        return this._apellido;
    }

    set apellido(value) {
        this._apellido = value;
    }

    get edad() {
        return this._edad;
    }

    set edad(value) {
        this._edad = value;
    }

    get ID() {
        return this._ID;
    }

    constructor(nombre, apellido="", edad=0) {
        this._ID = ++ Persona.contador
        this._nombre = nombre
        this._apellido = apellido
        this._edad = edad
    }

    toString(){
        return `ID: ${this._ID}\nNombre Completo: ${this._nombre} ${this._apellido}\nEdad: ${this._edad}`
    }

}