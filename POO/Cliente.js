class Cliente extends Persona{
    static contador = 0;
    static _cliente_ID;
    _cliente_fecha_registro;

    get cliente_ID() {
        return this._cliente_ID;
    }

    static get cliente_fecha_registro() {
        return this._cliente_fecha_registro;
    }

    static set cliente_fecha_registro(value) {
        this._cliente_fecha_registro = value;
    }

    constructor(nombre, apellido="", edad=0, fecha_registro = new Date()){
        super(nombre, apellido, edad);
        this._cliente_ID = ++ Cliente.contador;
        this._cliente_fecha_registro = fecha_registro;
    }

    toString() {
        return `${super.toString()}\nNo Cliente: ${this._cliente_ID}\nFecha de registro: ${this._cliente_fecha_registro}`;
    }
}