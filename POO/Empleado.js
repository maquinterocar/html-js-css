class Empleado extends Persona{
    static contador = 0;
    static _empleado_ID;
    _empleado_sueldo;

    get empleado_ID() {
        return this._empleado_ID;
    }
    get sueldo() {
        return this._empleado_sueldo;
    }

    set sueldo(value) {
        this._empleado_sueldo = value;
    }

    constructor(nombre, apellido="", edad=0,sueldo = 0) {
        super(nombre, apellido, edad);
        this._empleado_ID = ++ Empleado.contador;
        this._empleado_sueldo = sueldo
    }

    toString() {
        return `${super.toString()}\nNo. Empleado: ${this._empleado_ID}\nSueldo: ${this._empleado_sueldo}`;
    }
}