const BR=()=>{return document.createElement("br")};
const TR=()=>{return document.createElement("tr")};
const TH=()=>{return document.createElement("th")};
const TD=(value)=>{let td=document.createElement("td");td.innerText=value;return td;};
const TROW=(values)=>{
    let tr = TR();
    for (let value of values){
        console.log(value);
        tr.append(TD(value));
    }
    return tr;
};
const P=(value)=>{let p=document.createElement("p");p.innerText=value;return p;};
const TABLE=(values)=>{
    let table=document.createElement("table");
    if(values.length <= 0){
        return false;
    }
    let keys = Object.keys(values[0]);
    let captions = [];
    for (let row of keys){
        row = row.replace('_','')
        captions.push(row);
    }
    let tr = TR();
    for (let caption of captions){
        let th = TH();
        th.innerText = caption.toUpperCase();
        tr.append(th);
    }
    table.append(tr);
    for (const row of values){
        let row_values = Object.values(row);
        tr = TROW(row_values);
        table.append(tr);
    }
    return table;
}